from django.db import connection, IntegrityError
from django.contrib import messages
from django.shortcuts import render, redirect, reverse
from datetime import datetime

def transaksi(request, pk):
    userEmail = request.session["user_email"]
    trans = pk.split(';')
    waktuT = trans[0]
    tanggalT =trans[1]
    with connection.cursor() as c:
        namaKelasQ = f"""SELECT DISTINCT d.nama_kelas, d.nama_pemilik_tiket, e.nama, t.total_bayar 
                        FROM transaksi t, daftar_tiket d, event e 
                        WHERE t.email_pengunjung = '{userEmail}' AND t.email_pengunjung = d.email_pengunjung 
                        AND d.id_event = e.id_event AND t.waktu = '{waktuT}' AND t.tanggal = '{tanggalT}'
                        AND d.waktu_transaksi = '{waktuT}' AND d.tanggal_transaksi = '{tanggalT}'"""
        c.execute(namaKelasQ)
        kelasTransaksi = fetch(c)
        sumQ = "SELECT SUM(total_bayar) as total FROM transaksi WHERE email_pengunjung = '" + userEmail + "' AND waktu = '" + waktuT + "'"
        c.execute(sumQ)
        totalB = fetch(c)
        dompetQ = "SELECT DISTINCT d.nama, d.saldo, d.no_akun FROM transaksi t, dompet_digital d WHERE t.email_pengunjung = '" + userEmail + "' AND t.no_dompet_digital = d.no_akun "
        c.execute(dompetQ)
        dompetD = fetch(c)
        
        context = {
            'waktuT': waktuT,
            'tanggalT': tanggalT,
            'kelasTransaksi': kelasTransaksi,
            'totalB': totalB[0]['total'],
            'dompetD': dompetD
            }
    return render(request, 'transaksi.html', context)

def konfirmasiTransaksi(request, pk):
    userEmail = request.session["user_email"]
    trans = pk.split(';')
    waktuT = trans[0]
    tanggalT =trans[1]
    cursor = connection.cursor()
    dompetQ = "SELECT DISTINCT d.nama, d.saldo, d.no_akun FROM transaksi t, dompet_digital d WHERE t.email_pengunjung = '" + userEmail + "' AND t.no_dompet_digital = d.no_akun "
    cursor.execute(dompetQ)
    dompetD = fetch(cursor)
    sumQ = "SELECT SUM(total_bayar) as total FROM transaksi WHERE email_pengunjung = '" + userEmail + "' AND waktu = '" + waktuT + "'"
    cursor.execute(sumQ)
    totalB = fetch(cursor)
    countQ = "SELECT (saldo-SUM(total_bayar)) as saldoa FROM transaksi t, dompet_digital d WHERE t.email_pengunjung = '" + userEmail + "' AND no_dompet_digital = no_akun AND t.waktu = '" + waktuT + "'GROUP BY saldo"
    cursor.execute(countQ)
    counter = fetch(cursor)

    context = {
        'waktuT': waktuT,
        'tanggalT': tanggalT,
        'dompetD': dompetD,
        'totalB': totalB[0]['total'],
        'counter': counter[0]['saldoa']
        }
    if(request.method == "POST"):

        cursor.execute("UPDATE dompet_digital SET saldo = %s WHERE no_akun = %s", [counter[0]['saldoa'], dompetD[0]['no_akun']])
        return redirect('pesanan:transaksi_saya')
        
    return render(request, 'konfirmasi_transaksi.html', context)

def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]
