from django.urls import path
from . import views

app_name = 'transaksi'

urlpatterns = [
    path('<str:pk>', views.transaksi, name='transaksi'),
    path('konfirmasi-transaksi/<str:pk>', views.konfirmasiTransaksi, name='konfirmasiTransaksi'),
]
