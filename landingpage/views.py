from django.shortcuts import render

# Create your views here.
def home(request):
    context = {
        'auth' : False,
        'role': None
    }

    if request.session.get('user_email'):
        context['auth'] = True,
        context['role'] = request.session.get('role')

    return render(request, 'landingpage/index.html', context)