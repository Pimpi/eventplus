from django.urls import path
from . import views

app_name = 'testimoni'

urlpatterns = [
    path('buat-testimoni/', views.buatTestimoni, name='buat-testimoni'),
    path('daftar-testimoni/', views.daftarTestimoni, name='daftar-testimoni'),
    path('edit-testimoni/', views.editTestimoni, name='edit-testimoni'),
    path('edit-testimoni/hapus-testimoni/', views.hapusTestimoni, name='hapus-testimoni'),
]