from django.db import connection, IntegrityError
from django.contrib import messages
from django.shortcuts import render, redirect, reverse
from datetime import datetime

def buatTestimoni(request):
    if "role" not in request.session:
            return redirect('user:login')
    
    writerEmail = request.session["user_email"]
    if(request.method == 'POST'):
        cursor = connection.cursor()
        dropdown = request.POST.get('dropdown')
        rating = request.POST.get('rating')
        foto = request.POST.get('foto')
        isi = request.POST.get('isi')

        writerDaftarT = "SELECT * FROM daftar_tiket dt, transaksi t WHERE t.email_pengunjung = '" + writerEmail + "' AND dt.waktu_transaksi = t.waktu"
        cursor.execute(writerDaftarT)
        wdt = fetch(cursor)
        
        # try:
        cursor.execute("INSERT INTO testimoni values (%s, %s, %s, %s, %s, %s, %s, %s)", [writerEmail, wdt.email_pemilik_tiket, wdt.waktu, wdt.tanggal, datetime.now(), foto, isi, rating])
        return redirect('testimoni:daftar-testimoni')
		# except IntegrityError:
		# 	msg = "Failed to send message."
		# 	return render(request, "tulisan_create.html", context={'message': msg})
    
    with connection.cursor() as c:
        namaEventQ = "SELECT DISTINCT nama FROM event ORDER BY nama ASC"
        c.execute(namaEventQ)
        listNamaE = fetch(c)
    
        context = {
            'listNamaE': listNamaE
            }
    return render(request,'buat_testimoni.html', context)


def daftarTestimoni(request):
    if "role" not in request.session:
            return redirect('user:login')

    writerEmail = request.session["user_email"]
    with connection.cursor() as c:
        testimoniQuery = f"""SELECT e.nama, t.email_pengunjung, t.email_pemilik_tiket, t.waktu_transaksi, t.tanggal_transaksi, timestamp_testimoni, t.foto, t.isi 
                            FROM testimoni t, daftar_tiket d, event e 
                            WHERE e.id_event = d.id_event AND d.email_pengunjung = '{writerEmail}' """
        c.execute(testimoniQuery)
        list_tulisan_testimoni = fetch(c)
        context = {
            'list_tulisan_drafted': list_tulisan_testimoni
            }
    return render(request, 'daftar_testimoni.html', context)

def editTestimoni(request):
    if "role" not in request.session:
            return redirect('user:login')

    writerEmail = request.session["user_email"]
    if(request.method == "POST"):
        rating = request.POST.get("rating")
        foto = request.POST.get("foto")
        isi = request.POST.get("isi")
        try:
            cursor = connection.cursor()
            data = [rating, foto, isi, writerEmail]
            cursor.execute("UPDATE testimoni SET rating = %s, foto = %s, isi = %s WHERE email_pengunjung = %s", data)
            return redirect('testimoni:daftar-testimoni')
        except IntegrityError:
            msg = "error"
            return render(request, "tulisan_edit.html", context={'message': msg})
    else:
        cursor = connection.cursor()
        query = "SELECT e.nama, t.rating, t.foto, t.isi FROM testimoni t, daftar_tiket d, event e WHERE e.id_event = d.id_event AND d.email_pengunjung = '" + writerEmail + "' "
        cursor.execute(query)
        existingTestimoni = fetch(cursor)[0]
        return render(request, 'edit_testimoni.html', {
            "existingTestimoni" : existingTestimoni})

def hapusTestimoni(request):
    writerEmail = request.session["user_email"]
    if(request.method == "POST"):
        cursor = connection.cursor()
        query = "DELETE FROM testimoni WHERE email_pengunjung = '" + writerEmail + "'"
        cursor.execute(query)
        return redirect("testimoni:daftar-testimoni")
    else:
        return render(request, 'hapus_testimoni.html', {})

def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]