from django.urls import path
from .views import *

app_name = 'event'

urlpatterns = [
    path('', daftar_event, name='daftar_event'),
    path('my_event', my_event, name='my_event'),
    path('create/', create, name='create_event'),
    path('update/<str:id>/', update, name='update_event'),
    path('delete/<str:id>/', delete, name='delete_event'),
    path('deleteconfirm/<str:id>/',confirmDelete, name='confirm_delete'),
    path('getLokasi/<str:id>/', getLokasi, name='get_lokasi'),
    path('getKelasTiket/<str:id>/', getKelasTiket, name='get_kelas_tiket')
]
