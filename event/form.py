from django import forms
from django.forms import formset_factory
from .connection import *

class UpdateEvent(forms.Form):
    nama = forms.CharField(label='Nama Event', required=True)
    desc = forms.CharField(required=True, widget=forms.Textarea)
    tema = forms.MultipleChoiceField(widget = forms.CheckboxSelectMultiple, choices = [
        ('Welcome','Welcome'), ('Aerial','Aerial'), ('Cook-off','Cook-off'), ('Garden','Garden'), ('Tea','Tea'),
        ('Stunt','Stunt'), ('Aroma','Aroma'), ('Arts','Arts'), ('Art Tour','Art Tour'), ('Digital','Digital'),
        ('Interact','Interact'), ('Augmented','Augmented'), ('Influencer','Influencer'), ('Games','Games'), ('Live','Live'),
        ('Throwback','Throwback'), ('Animal','Animal'), ('Back','Back'), ('Campfire','Campfire'), ('Carnival','Carnival'),
        ('Celebrity','Celebrity'), ('Community','Community'), ('Dance','Dance'), ('DIY Music','DIY Music'), ('Domestic','Domestic'),
        ('Drive-in','Drive-in'), ('Fashion','Fashion'), ('Festival','Festival'), ('Fortune','Fortune'), ('Hot Air','Hot Air'),
        ('Movie','Movie'), ('Open Mic','Open Mic'), ('Playground','Playground'), ('Play','Play'), ('Roast','Roast'), ('Slumber','Slumber'),
        ('Treasure','Treasure'), ('Trivia','Trivia'), ('Masquerade','Masquerade'), ('Food','Food')
        ])
    tanggal_mulai = forms.DateField(widget = forms.SelectDateWidget)
    tanggal_selesai = forms.DateField(widget = forms.SelectDateWidget)
    jam_mulai = forms.TimeField(required=True)
    jam_selesai = forms.TimeField(required=True)
    tipe = forms.ChoiceField(choices = [
        ('0','Konser'), ('1','Konferensi'), ('2','Pameran'), ('3', 'Workshop'), 
        ('4', 'Festival'), ('5', 'Reuni'), ('6', 'Seminar'), 
        ('7', 'Bazaar'), ('8', 'Pernikahan'), ('9','Wisuda') 
    ])

class LokasiEvent(forms.Form):
    nama_gedung = forms.CharField(required=True)
    kota = forms.CharField(required=True)
    alamat = forms.CharField(required=True)

LokasiEventSet = formset_factory(LokasiEvent, extra=1)

class KelasTiketEvent(forms.Form):
    nama_kelas = forms.CharField(required=True)
    tarif = forms.IntegerField(required=True)
    kapasitas = forms.IntegerField(required=True)

KelasTiketEvent = formset_factory(KelasTiketEvent, extra=1)

