from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.http import JsonResponse
from .connection import *
from datetime import datetime
from .form import *

# Create your views here.
def daftar_event(request):
    if request.GET.get('q'):
        event = search(request.GET['q'])
    else:    
        event = daftarEvent()
    paginator = Paginator(event, 10)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    role = None
    email = None
    auth = False
    if request.session.get('user_email'):
        role = request.session['role']
        email = request.session['user_email']
        auth = True
    context = {
        'auth':auth,
        'page':page_obj,
        'role':role,
        'email':email,
        'time':datetime.now()
    }

    return render(request, 'event/daftar_event.html', context)

def my_event(request):
    if not (request.session['user_email'] or request.session['role']=='Pengunjung'):
        return redirect('/')
    
    event = myEvent(request.session['user_email'])
    context = {
        'auth':True,
        'role':request.session['role'],
        'event':event,
        'time':datetime.now()
    }
    return render(request, 'event/my_event.html', context)

def create(request):
    if not (request.session['user_email'] or request.session['role']=='Pengunjung'):
        return redirect('/')

    form = UpdateEvent(request.POST or None)
    lokasiFormSet = LokasiEventSet(request.POST or None)
    kelasTiketFormSet = kelasTiketEvent(request.POST or None)
    context = {
        'auth':True,
        'role':request.session['role'],
        'form':form,
        'lokasiForm':lokasiFormSet,
        'kelasTiketForm':kelasTiketFormSet,
    }

    if request.method=='POST' and form.is_valid():
        createEvent(form.cleaned_data, request.session['user_email'])
        return redirect('/event/my_event')
    return render(request, 'event/create1.html', context)

def update(request, id):
    if not (request.session['user_email'] or request.session['role']=='Pengunjung'):
        return redirect('/')
    event = currentEvent(id)
    tema = temaEvent(id)
    listTema = []
    for t in tema:
        listTema.append(''.join(t))
    form = UpdateEvent(request.POST or None, initial= {
        'nama':event[1],
        'tanggal_mulai':event[2],
        'tanggal_selesai':event[3],
        'jam_mulai':event[4],
        'jam_selesai':event[5],
        'desc':event[6],
        'tipe':event[9],
        'tema':listTema,
    })
    context = {
        'auth':True,
        'role':request.session['role'],
        'form':form,
        'id':id
    }
 
    if request.method=='POST':
        a = request.POST.getlist('dataKelasTiket[]')
        b = request.POST.getlist('dataGedung[]')
        print(request.POST.__dict__)
        c = {
            'nama':request.POST['nama'],
            'tanggal_mulai':request.POST['tanggal_mulai'],
            'tanggal_selesai':request.POST['tanggal_selesai'],
            'jam_mulai':request.POST['jam_mulai'],
            'jam_selesai':request.POST['jam_selesai'],
            'desc':request.POST['desc'],
            'tipe':request.POST['tipe']
        }
        print('test')
        print(a)
        print(b)
        print(c)
        updateEvent(c, id)

        return redirect('/event/my_event')
    return render(request, 'event/update.html', context)

def delete(request, id):
    if not (request.session['user_email'] or request.session['role']=='Pengunjung'):
        return redirect('/')

    context = {
        'auth':True,
        'role':request.session['role'],
        'id':id
    }
    return render(request, 'event/delete.html', context)

def confirmDelete(request, id):
    if not (request.session['user_email'] or request.session['role']=='Pengunjung'):
        return redirect('/')
        
    deleteEvent(id)
    return redirect('/')

def getLokasi(request, id):
    lokasi = lokasiEvent(id)
    arrDict = []
    for l in lokasi:
        arrDict.append({
            'nama_gedung':l.nama_gedung,
            'kota':l.kota,
            'alamat':l.alamat
        })
    dataDict = {'data':arrDict}
    return JsonResponse(dataDict)

def getKelasTiket(request, id):
    kelasTiket = kelasTiketEvent(id)
    arrDict = []
    for kt in kelasTiket:
        arrDict.append({
            'nama_kelas':kt.nama_kelas,
            'tarif':kt.tarif,
            'kapasitas':kt.kapasitas
        })
    dataDict = {'data':arrDict}
    return JsonResponse(dataDict)