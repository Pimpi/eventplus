from django.db import connection
from collections import namedtuple

def daftarEvent():
    with connection.cursor() as cursor:
        cursor.execute("""
        (select eventplus.event.id_event, nama_tema, eventplus.event.nama, nama_organizer, nama_gedung,
        eventplus.event.tanggal_mulai, eventplus.event.tanggal_selesai, 
        eventplus.event.jam_mulai, eventplus.event.jam_selesai, 
        eventplus.event.kapasitas_total_tersedia, eventplus.event.email_organizer from eventplus.event 
        inner join (select eventplus.tema.id_event, 
        string_agg(eventplus.tema.nama_tema, ', ' order by eventplus.tema.nama_tema) as "nama_tema" 
        from eventplus.tema group by eventplus.tema.id_event) as t 
        on eventplus.event.id_event=t.id_event
        inner join (select eventplus.individu.email, concat(eventplus.individu.nama_depan, ' ', 
        eventplus.individu.nama_belakang) as "nama_organizer"
        from eventplus.individu group by eventplus.individu.email) as i
        on eventplus.event.email_organizer=i.email
        inner join (select p.id_event, string_agg(p.nama_gedung, ', ' order by p.nama_gedung) as "nama_gedung" 
        from (select eventplus.lokasi_penyelenggaraan_event.id_event, eventplus.lokasi.nama_gedung 
        from eventplus.lokasi, eventplus.lokasi_penyelenggaraan_event 
        where eventplus.lokasi.kode=eventplus.lokasi_penyelenggaraan_event.kode_lokasi) as p 
        group by p.id_event) as l 
        on eventplus.event.id_event=l.id_event)
        union
        (select eventplus.event.id_event, nama_tema, eventplus.event.nama, eventplus.perusahaan.nama as nama_organizer, nama_gedung,
        eventplus.event.tanggal_mulai, eventplus.event.tanggal_selesai,
        eventplus.event.jam_mulai, eventplus.event.jam_selesai,
        eventplus.event.kapasitas_total_tersedia, eventplus.event.email_organizer from eventplus.event
        inner join (select eventplus.tema.id_event, 
        string_agg(eventplus.tema.nama_tema, ', ' order by eventplus.tema.nama_tema) as "nama_tema"
        from eventplus.tema group by eventplus.tema.id_event) as t
        on eventplus.event.id_event=t.id_event
        inner join eventplus.perusahaan 
        on eventplus.perusahaan.email=eventplus.event.email_organizer
        inner join (select p.id_event, string_agg(p.nama_gedung, ', ' order by p.nama_gedung) as "nama_gedung" 
        from (select eventplus.lokasi_penyelenggaraan_event.id_event, eventplus.lokasi.nama_gedung 
        from eventplus.lokasi, eventplus.lokasi_penyelenggaraan_event 
        where eventplus.lokasi.kode=eventplus.lokasi_penyelenggaraan_event.kode_lokasi) as p 
        group by p.id_event) as l 
        on eventplus.event.id_event=l.id_event);
        """)
        row = namedtuplefetchall(cursor)
    return row

def myEvent(email):
    with connection.cursor() as cursor:
        cursor.execute("""
        select eventplus.event.id_event, nama_tema, eventplus.event.nama, nama_gedung, 
        eventplus.event.tanggal_mulai, eventplus.event.tanggal_selesai,
        eventplus.event.jam_mulai, eventplus.event.jam_selesai,
        eventplus.event.kapasitas_total_tersedia 
        from eventplus.event
        inner join (select eventplus.tema.id_event, 
        string_agg(eventplus.tema.nama_tema, ', ' order by eventplus.tema.nama_tema) as "nama_tema"
        from eventplus.tema group by eventplus.tema.id_event) as t
        on eventplus.event.id_event=t.id_event
        inner join (select p.id_event, string_agg(p.nama_gedung, ', ' order by p.nama_gedung) as "nama_gedung"
        from (select eventplus.lokasi_penyelenggaraan_event.id_event, eventplus.lokasi.nama_gedung from eventplus.lokasi,eventplus.lokasi_penyelenggaraan_event
        where eventplus.lokasi.kode=eventplus.lokasi_penyelenggaraan_event.kode_lokasi) as p group by p.id_event) as l
        on eventplus.event.id_event=l.id_event
        where eventplus.event.email_organizer=%s;
        """, [email])
        row = namedtuplefetchall(cursor)
    return row

def deleteEvent(id):
    with connection.cursor() as cursor:
        cursor.execute("delete from eventplus.event where eventplus.event.id_event=%s;", [id])

def createEvent(data, email):
    with connection.cursor() as cursor:
        cursor.execute("select eventplus.event.id_event from eventplus.event order by eventplus.event.id_event desc limit 1;")
        id_event = cursor.fetchone()
        num = str((int(id_event[0][5:])+1))
        if len(num) == 1:   
            num = '0' + num
        id_event = 'EVENT' + num
        cursor.execute(
            """
            insert into eventplus.event (id_event, nama, tanggal_mulai, tanggal_selesai, jam_mulai, jam_selesai, deskripsi, id_tipe, email_organizer) 
            values (%s, %s, %s, %s, %s, %s, %s, %s, %s);
            """, [
                id_event, data['nama'], str(data['tanggal_mulai']).replace("-", "/"), str(data['tanggal_selesai']).replace("-", "/"), 
                str(data['jam_mulai']), str(data['jam_selesai']), data['desc'], 'TIPEEVENT' + data['tipe'], email
                ])
        for tema in data["tema"]:
            cursor.execute("insert into eventplus.tema values (%s, %s)", [id_event, tema])

def updateEvent(data, id):
    with connection.cursor() as cursor:
        cursor.execute("update eventplus.event set nama=%s, tanggal_mulai=%s, tanggal_selesai=%s, jam_mulai=%s, jam_selesai=%s, deskripsi=%s, id_tipe=%s where id_event=%s;", [
            data['nama'], str(data['tanggal_mulai']).replace("-", "/"), str(data['tanggal_selesai']).replace("-", "/"),
            str(data['jam_mulai']), str(data['jam_selesai']), data['desc'], 'TIPEEVENT' + data['tipe'], id
        ])
        cursor.execute("delete from eventplus.tema where eventplus.tema.id_event=%s;", [id])
        for tema in data["tema"]:
            cursor.execute("insert into eventplus.tema values (%s, %s);", [id, tema])

def lokasiEvent(id):
    with connection.cursor() as cursor:
        cursor.execute("""
        select * 
        from eventplus.lokasi 
        join eventplus.lokasi_penyelenggaraan_event
        on eventplus.lokasi.kode=eventplus.lokasi_penyelenggaraan_event.kode_lokasi 
        where eventplus.lokasi_penyelenggaraan_event.id_event=%s;
        """, [id])
        row = namedtuplefetchall(cursor)
    return row

def kelasTiketEvent(id):
    with connection.cursor() as cursor:
        cursor.execute("""
        select *
        from eventplus.kelas_tiket
        where eventplus.kelas_tiket.id_event=%s;
        """, [id])
        row = namedtuplefetchall(cursor)
    return row

def currentEvent(id):
    with connection.cursor() as cursor:
        cursor.execute("select * from eventplus.event where eventplus.event.id_event=%s", [id])
        row = cursor.fetchone()
    return row

def temaEvent(id):
    with connection.cursor() as cursor:
        cursor.execute("select nama_tema from eventplus.tema where eventplus.tema.id_event=%s", [id])
        row = cursor.fetchall()
    return row

def search(q):
    where = "where lower(q.nama) like '%" + q.lower() + "%'" + " or "
    where += "to_char(q.tanggal_mulai, 'YYYY-mon-DD') like '%" + q.lower() + "%'" + " or "
    where += "to_char(q.tanggal_selesai, 'YYYY-mon-DD') like '%" + q.lower() + "%'" + " or "
    where += "lower(q.nama_organizer) like '%" + q.lower() + "%'" + " or "
    where += "lower(q.nama_gedung) like '%" + q.lower() + "%'"
    with connection.cursor() as cursor:
        cursor.execute("""
        select * from ((select eventplus.event.id_event, nama_tema, eventplus.event.nama, nama_organizer, nama_gedung,
        eventplus.event.tanggal_mulai, eventplus.event.tanggal_selesai, 
        eventplus.event.jam_mulai, eventplus.event.jam_selesai, 
        eventplus.event.kapasitas_total_tersedia, eventplus.event.email_organizer from eventplus.event 
        inner join (select eventplus.tema.id_event, 
        string_agg(eventplus.tema.nama_tema, ', ' order by eventplus.tema.nama_tema) as "nama_tema" 
        from eventplus.tema group by eventplus.tema.id_event) as t 
        on eventplus.event.id_event=t.id_event
        inner join (select eventplus.individu.email, concat(eventplus.individu.nama_depan, ' ', 
        eventplus.individu.nama_belakang) as "nama_organizer"
        from eventplus.individu group by eventplus.individu.email) as i
        on eventplus.event.email_organizer=i.email
        inner join (select p.id_event, string_agg(p.nama_gedung, ', ' order by p.nama_gedung) as "nama_gedung" 
        from (select eventplus.lokasi_penyelenggaraan_event.id_event, eventplus.lokasi.nama_gedung 
        from eventplus.lokasi, eventplus.lokasi_penyelenggaraan_event 
        where eventplus.lokasi.kode=eventplus.lokasi_penyelenggaraan_event.kode_lokasi) as p 
        group by p.id_event) as l 
        on eventplus.event.id_event=l.id_event)
        union
        (select eventplus.event.id_event, nama_tema, eventplus.event.nama, eventplus.perusahaan.nama as nama_organizer, nama_gedung,
        eventplus.event.tanggal_mulai, eventplus.event.tanggal_selesai,
        eventplus.event.jam_mulai, eventplus.event.jam_selesai,
        eventplus.event.kapasitas_total_tersedia, eventplus.event.email_organizer from eventplus.event
        inner join (select eventplus.tema.id_event, 
        string_agg(eventplus.tema.nama_tema, ', ' order by eventplus.tema.nama_tema) as "nama_tema"
        from eventplus.tema group by eventplus.tema.id_event) as t
        on eventplus.event.id_event=t.id_event
        inner join eventplus.perusahaan 
        on eventplus.perusahaan.email=eventplus.event.email_organizer
        inner join (select p.id_event, string_agg(p.nama_gedung, ', ' order by p.nama_gedung) as "nama_gedung" 
        from (select eventplus.lokasi_penyelenggaraan_event.id_event, eventplus.lokasi.nama_gedung 
        from eventplus.lokasi, eventplus.lokasi_penyelenggaraan_event 
        where eventplus.lokasi.kode=eventplus.lokasi_penyelenggaraan_event.kode_lokasi) as p 
        group by p.id_event) as l 
        on eventplus.event.id_event=l.id_event)) as q
        """ + where)
        row = namedtuplefetchall(cursor)
    return row

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]