from django.urls import path
from . import views

app_name = 'pesanan'

urlpatterns = [
	path('', views.pesan_tiket_view, name='pesan_tiket'),
	path('add/<str:pk>/', views.add_daftar_tiket_view, name='add_daftar_tiket'),
	path('delete/<str:pk>/', views.delete_daftar_tiket_view, name='delete_daftar_tiket'),
	path('buat/<str:pk>/', views.buat_transaksi_view, name='buat_transaksi'),
	path('cancel/<str:pk>/', views.cancel_pesan_tiket_view, name='cancel_pesan_tiket'),
	path('transaksi_saya/', views.transaksi_saya_view, name='transaksi_saya'),
	path('transaksi_saya/edit/<str:pk>/', views.edit_transaksi_view, name='edit_transaksi'),
	path('transaksi_saya/delete/<str:pk>/', views.delete_transaksi_view, name='delete_transaksi'),
]