from django.shortcuts import render, redirect
from datetime import datetime
from collections import namedtuple
from .connection import *
import random
import string

# Create your views here.
def pesan_tiket_view(request):
	if not request.session.get('user_email'):
		email = 'rison0@dagondesign.com'
	else:
		email = request.session.get('user_email')

	event_id = 'EVENT00'

	# Mengecek apakah ada Transaksi yang belum dipesan untuk event tersebut
	cekTransaksi = cekTransaksiQuery(email, event_id)

	# Jika tidak ditemukan Transaksi, buat Shared_Profit & Transaksi baru
	if list(cekTransaksi):
		waktu = cekTransaksi[0][1]
		tanggal = cekTransaksi[0][2]

	# Jika ditemukan Transaksi, ambil waktu dan tanggal
	else:
		waktu = datetime.now().strftime("%H:%M")
		tanggal = datetime.now().strftime("%Y-%m-%d")

		sp_id = generate_id()
		createNewSharedProfitQuery(sp_id)
		createNewTransaksiQuery(email, waktu, tanggal, sp_id)

	eventDetails = getEventDetailsQuery(event_id)
	allKelasTiket = getAllKelasTiketQuery(event_id)
	myDaftarTiket = getMyDaftarTiketQuery(email, waktu, tanggal, event_id)

	context = {
		'event': eventDetails[0],
		'kelasTiket': allKelasTiket,
		'daftarTiket': myDaftarTiket,
		'transaksi': cekTransaksi[0],
	}

	return render(request, 'pesan_tiket.html', context)

def add_daftar_tiket_view(request, pk):
	if not request.session.get('user_email'):
		email = 'rison0@dagondesign.com'
	else:
		email = request.session.get('user_email')

	event_id = 'EVENT00'
	email_pemilik = generate_email()

	daftar = pk.split(";")
	waktu = daftar[0]
	tanggal = daftar[1]
	nama_kelas_tiket = daftar[2]

	addDaftarTiketQuery(email, email_pemilik, waktu, tanggal, event_id, nama_kelas_tiket)
	return redirect('pesanan:pesan_tiket')

def delete_daftar_tiket_view(request, pk):
	if not request.session.get('user_email'):
		email = 'rison0@dagondesign.com'
	else:
		email = request.session.get('user_email')

	event_id = 'EVENT00'

	daftar = pk.split(";")
	email_pemilik = daftar[0]
	waktu = daftar[1]
	tanggal = daftar[2]
	nama_kelas_tiket = daftar[3]

	deleteDaftarTiketQuery(email, email_pemilik, waktu, tanggal, event_id, nama_kelas_tiket)
	return redirect('pesanan:pesan_tiket')

def buat_transaksi_view(request, pk):
	if not request.session.get('user_email'):
		email = 'rison0@dagondesign.com'
	else:
		email = request.session.get('user_email')

	daftar = pk.split(";")
	waktu = daftar[0]
	tanggal = daftar[1]

	buatTransaksiQuery(email, waktu, tanggal)
	return redirect('event:daftar_event')

def cancel_pesan_tiket_view(request, pk):
	if not request.session.get('user_email'):
		email = 'rison0@dagondesign.com'
	else:
		email = request.session.get('user_email')

	daftar = pk.split(";")
	waktu = daftar[0]
	tanggal = daftar[1]

	deleteTransaksiQuery(email, waktu, tanggal)
	return redirect('event:daftar_event')

def transaksi_saya_view(request):
	if not request.session.get('user_email'):
		email = 'rison0@dagondesign.com'
	else:
		email = request.session.get('user_email')

	allTransaksi = getAllTransaksiQuery(email)

	context = {
		'transaksi': allTransaksi,
	}
	return render(request, 'transaksi_saya.html', context)

def edit_transaksi_view(request, pk):
	if not request.session.get('user_email'):
		email = 'rison0@dagondesign.com'
	else:
		email = request.session.get('user_email')

	daftar = pk.split(";")
	waktu = daftar[0]
	tanggal = daftar[1]

	event = getEventQuery(email, waktu, tanggal)
	event_id = event[0][0]

	eventDetails = getEventDetailsQuery(event_id)
	allKelasTiket = getAllKelasTiketQuery(event_id)
	myDaftarTiket = getMyDaftarTiketQuery(email, waktu, tanggal, event_id)

	context = {
		'event': eventDetails[0],
		'kelasTiket': allKelasTiket,
		'daftarTiket': myDaftarTiket,
		'waktu': waktu,
		'tanggal': tanggal,
	}

	return render(request, 'update_tiket.html', context)

def delete_transaksi_view(request, pk):
	if not request.session.get('user_email'):
		email = 'rison0@dagondesign.com'
	else:
		email = request.session.get('user_email')

	daftar = pk.split(";")
	waktu = daftar[0]
	tanggal = daftar[1]

	deleteTransaksiQuery(email, waktu, tanggal)
	return redirect('pesanan:transaksi_saya')

def generate_id():
	number = random.randint(1, 9999)
	return 'SP{}{}'.format(datetime.now().strftime('%Y%m%d%H%M%S'), number)

def generate_email():
	email = "".join(random.choice(string.ascii_letters) for x in range(10))
	return (email + "@gmail.com")