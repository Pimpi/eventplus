from django.db import connection
from collections import namedtuple

def cekTransaksiQuery(email, event_id):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			SELECT t.*
			FROM TRANSAKSI t, DAFTAR_TIKET d, EVENT e
			WHERE t.email_pengunjung = %s
			AND t.status = 'Belum Dipesan'
			AND t.email_pengunjung = d.email_pengunjung
			AND d.id_event = %s;
			""", [email, event_id]
		)

		row = namedtuplefetchall(cursor)
	return row

def createNewSharedProfitQuery(sp_id):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			INSERT INTO SHARED_PROFIT VALUES
			(%s, 0);
			""", [sp_id]
		)

def createNewTransaksiQuery(email, waktu, tanggal, sp_id):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			SELECT d.no_akun
			FROM DOMPET_DIGITAL d
			WHERE d.email_pengunjung = %s;
			""", [email]
		)

		dompet = namedtuplefetchall(cursor)
		no_dompet = dompet[0]

		cursor.execute(
			"""
			INSERT INTO TRANSAKSI VALUES
			(%s, %s, %s, %s, 'Belum Dipesan', 0, %s);
			""", [email, waktu, tanggal, no_dompet, sp_id]
		)

def getEventDetailsQuery(event_id):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			SELECT te.nama_tema, e.nama, e.deskripsi, e.tanggal_mulai, e.tanggal_selesai, l.nama_gedung, l.kota, l.alamat, ti.nama_tipe
			FROM EVENT e, TEMA te, TIPE ti, LOKASI l, LOKASI_PENYELENGGARAAN_EVENT lpe
			WHERE te.id_event = e.id_event
			AND l.kode = lpe.kode_lokasi
			AND lpe.id_event = e.id_event 
			AND ti.id_tipe = e.id_tipe
			AND e.id_event = %s;
			""", [event_id]
		)
		row = namedtuplefetchall(cursor)
	return row

def getAllKelasTiketQuery(event_id):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			SELECT kt.*
			FROM EVENT e, KELAS_TIKET kt
			WHERE kt.id_event = e.id_event
			AND e.id_event = %s;
			""", [event_id]
		)
		row = namedtuplefetchall(cursor)
	return row

def getMyDaftarTiketQuery(email, waktu, tanggal, event_id):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			SELECT dt.*
			FROM EVENT e, DAFTAR_TIKET dt, TRANSAKSI t
			WHERE dt.id_event = e.id_event
			AND dt.email_pengunjung = t.email_pengunjung
			AND dt.waktu_transaksi = t.waktu
			AND dt.tanggal_transaksi = t.tanggal
			AND t.email_pengunjung = %s
			AND t.waktu = %s
			AND t.tanggal = %s
			AND e.id_event = %s;
			""", [email, waktu, tanggal, event_id]
		)
		row = namedtuplefetchall(cursor)
	return row

def addDaftarTiketQuery(email, email_pemilik, waktu, tanggal, event_id, nama_kelas_tiket):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			SELECT p.nama_depan
			FROM PENGUNJUNG p
			WHERE p.email = %s;
			""", [email]
		)
		row = namedtuplefetchall(cursor)
		nama = row[0].nama_depan

		cursor.execute(
			"""
			INSERT INTO DAFTAR_TIKET VALUES
			(%s, %s, %s, %s, %s, %s, %s)
			""", [email, email_pemilik, waktu, tanggal, nama, event_id, nama_kelas_tiket]
		)

def deleteDaftarTiketQuery(email, email_pemilik, waktu, tanggal, event_id, nama_kelas_tiket):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			DELETE FROM DAFTAR_TIKET
			WHERE email_pengunjung = %s
			AND email_pemilik_tiket = %s
			AND waktu_transaksi = %s
			AND tanggal_transaksi = %s
			AND id_event = %s
			AND nama_kelas = %s;
			""", [email, email_pemilik, waktu, tanggal, event_id, nama_kelas_tiket]
		)

def buatTransaksiQuery(email, waktu, tanggal):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			UPDATE TRANSAKSI
			SET status = 'Incomplete'
			WHERE status = 'Belum Dipesan'
			AND email_pengunjung = %s
			AND waktu = %s
			AND tanggal = %s;
			""", [email, waktu, tanggal]
		)

def getAllTransaksiQuery(email):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			SELECT t.*
			FROM PENGUNJUNG p, TRANSAKSI t
			WHERE t.email_pengunjung = p.email
			AND p.email = %s;
			""", [email]
		)
		row = namedtuplefetchall(cursor)
	return row

def deleteTransaksiQuery(email, waktu, tanggal):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			DELETE FROM TRANSAKSI
			WHERE email_pengunjung = %s
			AND waktu = %s
			AND tanggal = %s;
			""", [email, waktu, tanggal]
		)

def getEventQuery(email, waktu, tanggal):
	with connection.cursor() as cursor:
		cursor.execute(
			"""
			SELECT e.*
			FROM EVENT e, DAFTAR_TIKET d, TRANSAKSI t
			WHERE e.id_event = d.id_event
			AND d.email_pengunjung = t.email_pengunjung
			AND d.waktu_transaksi = t.waktu
			AND d.tanggal_transaksi = t.tanggal
			AND t.email_pengunjung = %s
			AND t.waktu = %s
			AND t.tanggal = %s;
			""", [email, waktu, tanggal]
		)
		row = namedtuplefetchall(cursor)
	return row

def namedtuplefetchall(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]