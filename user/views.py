from django.shortcuts import render, redirect
from django.db import InternalError, IntegrityError
from .connection import *
from .form import *

# Create your views here.
def login(request):
    form = CreateLogin(request.POST or None)
    context = {
        'form':form,
        'error':'Username/Password gagal. Silahkan ulangi Kembali dengan informasi yang benar',
        'flag':False
    }

    if request.method=='POST':
        a = loginCheck(form.data['email'], form.data['password'])

        # Jika kombinasi email dan password tidak ditemukan
        if (a == None):
            context['flag']=True
            return render(request, 'user/login.html', context)

        # Periksa rolenya dan set sessionnya
        b = roleCheck(form.data['email'])
        if (b == None):
            request.session['user_email'] = a[0]
            request.session['role'] = 'Pengunjung'
        else:
            request.session['user_email'] = a[0]
            request.session['role'] = 'Organizer'
        return redirect('/')

    return render(request, 'user/login.html', context)

def register(request):
    return render(request, 'user/register.html')

def registerPengunjung(request):
    form = CreateRegisterPengunjung(request.POST or None)
    context = {
        'form': form,
        'error': 'Registrasi Gagal. Silahkan lakukan pengecekkan kembali terhadap inputan anda',
        'flag': False,
    }

    if request.method=='POST':
        try:
            a = registerCheckPengunjung(form.data)        
        except (InternalError, IntegrityError):
            context['flag']=True
            return render(request, 'user/register_pengunjung.html', context)
        
        return redirect(login)
    
    return render(request, 'user/register_pengunjung.html', context)

def registerOrganizer(request):
    form = CreateRegisterOrganizer(request.POST or None)
    context = {
        'form': form,
        'error': 'Registrasi Gagal. Silahkan lakukan pengecekkan kembali terhadap inputan anda',
        'flag': False,
    }
    if request.method=='POST':
        try:
            a = registerCheckOrganizer(form.data)
        except (InternalError, IntegrityError):
            context['flag']=True
            return render(request, 'user/register_pengunjung.html', context)

        return redirect(login)
        
    return render(request, 'user/register_organizer.html', context)

def logout(request):
    request.session.flush()
    return redirect('/')