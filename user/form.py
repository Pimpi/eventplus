from django import forms

class CreateLogin(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(widget=forms.PasswordInput)

class CreateRegisterPengunjung(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(widget=forms.PasswordInput)
    nama = forms.CharField(required=True)
    alamat = forms.CharField(required=True, widget=forms.Textarea)

class CreateRegisterOrganizer(forms.Form):
    role = forms.CharField(widget=forms.RadioSelect(choices=[('id','Individu'),('pr','Perusahaan')]))
    email = forms.EmailField(required=True)
    password = forms.CharField(widget=forms.PasswordInput)
    no = forms.CharField(required=True, label='NPWP/No KTP')
    nama = forms.CharField(required=True)