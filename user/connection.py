from django.db import connection

def loginCheck(email, password):
    with connection.cursor() as cursor:
        cursor.execute("select * from eventplus.pengguna where eventplus.pengguna.email=%s and eventplus.pengguna.password=%s", [email, password])
        row = cursor.fetchone()
    return row

def roleCheck(email):
    with connection.cursor() as cursor:
        cursor.execute("select eventplus.organizer.email from eventplus.organizer where eventplus.organizer.email=%s", [email])
        row = cursor.fetchone()
    return row

def registerCheckOrganizer(data):
    with connection.cursor() as cursor:
        cursor.execute("insert into eventplus.pengguna values (%s, %s)", [data['email'], data['password']])
        cursor.execute("insert into eventplus.organizer values (%s, %s)", [data['email'], data['no']])
        if data['role'] == 'id':
            nama = data['nama'].strip().split()
            if nama.length() == 1:
                cursor.execute("insert into eventplus.individu values (%s, %s, %s, %s)", [data['email'], data['no'], nama[0], nama[0]])
            else:
                cursor.execute("insert into eventplus.individu values (%s, %s, %s, %s)", [data['email'], data['no'], nama[0], nama[1]])
        else:
            cursor.execute("insert into eventplus.perusahaan values (%s, %s)", [data['email'], data['no']])

def registerCheckPengunjung(data):
    with connection.cursor() as cursor:
        cursor.execute("insert into eventplus.pengguna values (%s, %s)", [data['email'], data['password']])
        nama = data['nama'].split()
        if nama.length() == 1:
            cursor.execute("insert into eventplus.pengunjung values (%s, %s, %s, %s)", [data['email'], nama[0], nama[0], data['alamat']])
        else:
            cursor.execute("insert into eventplus.pengunjung values (%s, %s, %s, %s)", [data['email'], nama[0], nama[1], data['alamat']])