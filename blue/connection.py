from django.db import connection

def getCustomer(email):
    with connection.cursor() as cursor:
        cursor.execute("select eventplus.pengunjung.email from eventplus.pengunjung where eventplus.pengunjung.email=%s", [email])
        row = cursor.fetchone()
    return row

def getIndividual(email):
    with connection.cursor() as cursor:
        cursor.execute("select eventplus.individu.email from eventplus.individu where eventplus.individu.email=%s", [email])
        row = cursor.fetchone()
    return row

def getCompany(email):
    with connection.cursor() as cursor:
        cursor.execute("select eventplus.perusahaan.email from eventplus.perusahaan where eventplus.perusahaan.email=%s", [email])
        row = cursor.fetchone()
    return row

def getUser(email):
    with connection.cursor() as cursor:
        cursor.execute("select eventplus.pengguna.password from eventplus.pengguna where eventplus.pengguna.email=%s", [email])
        row = cursor.fetchone()
    return row

def getOrganizer(email):
    with connection.cursor() as cursor:
        cursor.execute("select eventplus.organizer.npwp from eventplus.organizer where eventplus.organizer.email=%s", [email])
        row = cursor.fetchone()
    return row

def getDigitalWallet(email):
    with connection.cursor() as cursor:
        cursor.execute("select * from eventplus.dompet_digital where eventplus.dompet_digital.email=%s", [email])
        row = cursor.fetchone()
    return row