from django.shortcuts import render, redirect
from django.db import InternalError, IntegrityError
from .connection import *

# Create your views here.

def profil(request):
    if not request.session.get('user_email'):
        return redirect('/')

    email = request.session['user_email']
    passw = getUser(email)

    profile = getCustomer(email)
    if (profile != None):
        context = {
            'profile': profile,
            'passw': passw
        }
        return render(request, 'blue/profilPengunjung.html', context)

    organizer = getOrganizer(email)

    profile = getIndividual(email)
    if (profile != None):
        context = {
            'profile': profile,
            'passw': passw,
            'organizer': organizer
        }
        return render(request, 'blue/profilIndividu.html', context)

    profile = getCompany(email)
    if (profile != None):
        context = {
            'profile': profile,
            'passw': passw,
            'organizer': organizer
        }
        return render(request, 'blue/profilPerusahaan.html', context)

def editProfilPengunjung(request):
    return render(request, 'blue/editProfilPengunjung.html')

def editProfilIndividu(request):
    return render(request, 'blue/editProfilIndividu.html')

def editProfilPerusahaan(request):
    return render(request, 'blue/editProfilPerusahaan.html')

def dompetDigital(request):
    context = {
        'wallet': getDigitalWallet
    }
    return render(request, 'blue/dompetDigital.html', context)

def tambahSaldo(request):
    return render(request, 'blue/tambahSaldo.html')