from django.urls import path
from .views import *

app_name = 'blue'

urlpatterns = [
    path('profil', profil, name='profil'),
    path('editProfilPengunjung', editProfilPengunjung, name='editProfilPengunjung'),
    path('editProfilIndividu', editProfilIndividu, name='editProfilIndividu'),
    path('editProfilPerusahaan', editProfilPerusahaan, name='editProfilPerusahaan'),
    path('dompetDigital', dompetDigital, name='dompetDigital'),
    path('tambahSaldo', tambahSaldo, name='tambahSaldo'),
]
